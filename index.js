"use strict";

var glob = require("glob");
var ensureStringEndsWithSlash = require('ensure-string-endswith')('/');
var path = require('path');

var GitObjectIds = module.exports = function(pathToGitRepo, cb) {
	var callback = cb || function() {};
	return new Promise(function(resolve, reject) {
		var normalizedPath = ensureStringEndsWithSlash(path.normalize(pathToGitRepo));
		glob(normalizedPath+".git/objects/**/*", function (err, files) {
			if(err) {
				reject(err);
				callback(err);
			}
			var regEx = new RegExp("^.*([0-9a-f][0-9a-f])/([0-9a-f]{38})");
			var sha1s = files.filter(function(file) {
					return file.match(regEx) !== null;
				})
				.map(function(file) {
					return file.replace(regEx, '$1$2');
				});
			resolve(sha1s);
			callback(undefined, sha1s);
		});
	});
};

