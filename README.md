# git-objectids

> Get all object-ids from a git repository.

## API

### Callback
```
"use strict";

var gitObjectIds = require('git-objectids');

gitObjectIds("/path/to/git-repo/", function(err, files) {
/*
	err: undefined,
	files: [
		'd81cc0710eb6cf9efd5b920a8453e1e07157b6cd',
		'1783cee7467c5d9b5bbce02e349b6a0b3eb28126'
	]
*/
});

```
### Promise
```
"use strict";

var gitObjectIds = require('git-objectids');

gitObjectIds("/path/to/git-repo/")
.then(function(files) {
/*
	files: [
		'd81cc0710eb6cf9efd5b920a8453e1e07157b6cd',
		'1783cee7467c5d9b5bbce02e349b6a0b3eb28126'
	]
*/
});

```

## Caveat
Packs are not supported.

## License

MIT © [Christoph Häfner](http://christophhaefner.de)

