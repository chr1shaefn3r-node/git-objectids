var assert = require("assert");
var GitObjectIds = require('../index.js');
var mock = require('mock-fs');

suite('GitObjectIds', function() {
	test('globRegression', function(done) {
		mock({
			'/tmp/.git/objects/': {
				'31': {
					'243f623b41c4cb83e0c8d2827a661298a06db6': ''
				},
				'91': {
					'983f65fea893f89bd676d1d9b5166ea4812ef4': ''
				}
			}
		});
		var expected = [
			'31243f623b41c4cb83e0c8d2827a661298a06db6',
			'91983f65fea893f89bd676d1d9b5166ea4812ef4'
		];
		GitObjectIds("/tmp/", function(err, ids) {
			assert.ifError(err);
			assert.deepStrictEqual(ids, expected);
			done();
		});
	});

	after(function() {
		mock.restore();
	});
});

